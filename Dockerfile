FROM docker.io/mariadb/maxscale:6.1.3

RUN yum remove -y rsyslog monit && \
    yum clean all -y && \
    chmod g=u /etc/passwd && \
    chmod -R g=u /var/{lib,run,log,cache}/maxscale && \
    chgrp -R 0 /var/{lib,run,log,cache}/maxscale

COPY maxscale.cnf /etc/maxscale.cnf
COPY maxscale.cnf /home/maxscale.cnf

EXPOSE 6603 3306 3307 8003